import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class AnagramaTest {

	private Principal principal;
	
	@Before
	public void init() {
		principal = new Principal();
	}
	
	@Test
	public void umaLetraTest() {		
		List<String> esperado = new ArrayList<String>();
		esperado.add("A");
		
		List<String> resultado = principal.getAnagramas("A");
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void duasLetrasTest() {		
		List<String> esperado = new ArrayList<String>();
		esperado.add("AR");
		esperado.add("RA");		
		
		List<String> resultado = principal.getAnagramas("AR");
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void tresLetrasTest() {		
		List<String> esperado = new ArrayList<String>();
		esperado.add("PRE");
		esperado.add("PER");
		esperado.add("RPE");
		esperado.add("REP");
		esperado.add("EPR");
		esperado.add("ERP");		
		
		List<String> resultado = principal.getAnagramas("PRE");
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void quatroLetrasTest() {		
		List<String> esperado = new ArrayList<String>();
		esperado.add("TRES");
		esperado.add("TRSE");
		esperado.add("TSRE");
		esperado.add("TSER");
		esperado.add("TESR");
		esperado.add("TERS");
		esperado.add("RTES");
		esperado.add("RTSE");
		esperado.add("RSTE");
		esperado.add("RSET");
		esperado.add("REST");
		esperado.add("RETS");
		esperado.add("ETRS");
		esperado.add("ETSR");
		esperado.add("ESTR");
		esperado.add("ESRT");
		esperado.add("ERST");
		esperado.add("ERTS");
		esperado.add("STRE");
		esperado.add("STER");
		esperado.add("SETR");
		esperado.add("SERT");
		esperado.add("SRTE");
		esperado.add("SRET");		
		
		List<String> resultado = principal.getAnagramas("TRES");
		assertEquals(esperado, resultado);
	}
}

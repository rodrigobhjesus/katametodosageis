import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		System.out.println("Informar uma palavra para gerar o anagrama.");
		Scanner le = new Scanner(System.in);
		String  tLeitura = le.nextLine();
		
	}
	
	public List<String> combina(String pPalavra){
		List<String> retorno = new ArrayList<String>();
		if (pPalavra.length() == 1)
			retorno.add(pPalavra);
		else if (pPalavra.length() == 2){
			retorno.add(""+pPalavra.charAt(0)+pPalavra.charAt(1));
			retorno.add(""+pPalavra.charAt(1)+pPalavra.charAt(0));
		}	
		else{
			for (int i = 0; i < pPalavra.length(); i++) {
				retorno.addAll( combina(pPalavra.substring(0, i) ) );
			}
		}
		return retorno;
	}
	
	
	public List<String> getAnagramas(String palavra) {
		List<String> retorno = new ArrayList<String>();
		
		retorno.addAll(combina(palavra));
		return retorno;
		
	}

}
